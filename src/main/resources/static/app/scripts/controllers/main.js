'use strict';

/**
 * @ngdoc function
 * @name wwwApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wwwApp
 */
angular.module('wwwApp')
    .controller('MainCtrl', function ($scope, $interval, api) {

        // health check
        function hc() {
            api.healthCheck().then(function(err) {
                $scope.healthCheck = !!err;
            }, function() {
                $scope.healthCheck = false;
                $scope.trelloAuthed = undefined;
                $scope.userAuthed = undefined;
            })
        }
        hc();
        $interval(hc, 1000);

        function errorHandler(response) {
            switch (response.status) {
                case 400:
                    if(response.data && response.data.errorCode && response.data && response.data.errorCode == 4002) {
                        alert(response.data.error);
                    }
                    break;
                case 401:
                    if(response.data && response.data.error) {
                        $scope.trelloAuthed = false;
                        $scope.authorizeUrl = api.getAuthenticateTrelloUrl();
                    }

                    break;
                case 500:
                    console.log("Error");
                    console.log(response);
            }
        }

        $scope.updateUser = function(userToken) {
            window.localStorage.setItem("userToken", userToken);
            api.setToken(userToken);
            api.isAuthed().then(function(response) {
                $scope.userAuthed = response.data.data.authed;

                if($scope.userAuthed) {
                    api.getBoards().then(function(response) {
                        $scope.trelloAuthed = true;
                        $scope.boards = response.data.data;
                    }, errorHandler);
                } else {
                    $scope.trelloAuthed = false;
                }
            })
        };

        $scope.showCards = function(board) {
            api.getCards(board.id).then(function(response) {
                board.cards = response.data.data;
            });
        };

        $scope.comment = function(card) {
            var text = prompt("Comment");

            if (text) {
                api.comment(card.id, text).then(function() {
                    alert("Done");
                }, errorHandler);
            }
        };

        $scope.userToken = window.localStorage.getItem("userToken");
        if ($scope.userToken) {
            $scope.updateUser($scope.userToken);
        }


    })
    .factory('api', function($http) {

        var apiUrl = "http://localhost:8080";

        var self = this;

        function callApi(url, data) {
            var req = {
                url: apiUrl + url,
                headers: {
                    'X-Auth-Token': self.token
                }
            };

            if(data) {
                req.method = "POST";
                req.data = data;
            }

            return $http(req)
        }

        return {
            healthCheck : function() {
                return callApi("/healthCheck");
            },
            isAuthed : function() {
                return callApi("/authed");
            },
            setToken : function(token) {
                self.token = token;
            },
            getBoards : function() {
                return callApi("/trello/boards");
            },
            getCards : function(boardId) {
                return callApi("/trello/cards/" + boardId);
            },
            getAuthenticateTrelloUrl : function() {
                return apiUrl + "/trello/authorizeAndRedirect?ut=" + self.token;
            },
            comment : function(cardId, text) {
                return callApi("/trello/comment/" + cardId, { "text" : text });
            }
        }
});
