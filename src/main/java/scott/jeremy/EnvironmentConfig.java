package scott.jeremy;

/**
 * Application configuration.
 *
 * To override the default values, and an environment variable using the associated key
 */
public class EnvironmentConfig {
    public String getKey() {
        return envOrDefault("trelloKey", "60bf143f45f5fe9825c4d722a603c6b7");
    }

    public String getSecret() {
        return envOrDefault("trelloSecret", "3dbc2fd5b8e6a9234a1f2231550086e140d2e867aa4e96277dedf69bd5cbb365");
    }

    public String getHost() {
        return envOrDefault("host", "http://localhost:8080");
    }

    public String getRedisHostPort() {
        return envOrDefault("redisHostPort", "redis://localhost:6379");
    }

    public String getAuthCompleteCallback() {
        return envOrDefault("authCompleteCallback", "http://localhost:8080/app/index.html");
    }

    public String getAppName() {
        return envOrDefault("appName", "Commentator");
    }

    private String envOrDefault(String envVar, String defaultValue) {
        return System.getProperty(envVar) != null ? System.getProperty(envVar) : defaultValue;
    }

}
