package scott.jeremy;

import java.util.List;

public class Response {
    int status;
    List<String> errors;
    Object data;

    public Response data(Object data) {
        this.data = data;

        return this;
    }

    public int getStatus() {
        return status;
    }

    public List<String> getErrors() {
        return errors;
    }

    public Object getData() {
        return data;
    }
}
