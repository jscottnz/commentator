package scott.jeremy;

import com.google.common.cache.Cache;
import redis.clients.jedis.Jedis;
import scott.jeremy.trello.TrelloClient;

import java.util.Base64;

public class UserService {

    private static final String REDIS_PREFIX_FOR_USER_OAUTH_TOKEN = "oAuthToken_";

    private Cache<String /* userToken */, String /* oAuthToken */> oAuthCache;
    private final Jedis redis;

    public UserService(Cache<String, String> oAuthCache, Jedis redis) {
        this.oAuthCache = oAuthCache;
        this.redis = redis;
    }

    /*
    Add implementation for real authorization
     */
    public boolean isAuthorized(String token) {
        return false;
    }

    public void saveOAuthTokenForUser(String userToken, String oAuthToken) {
        redis.set(getRedisKeyForUserOAuthToken(userToken), encrypt(oAuthToken));
        oAuthCache.put(userToken, oAuthToken);
        redis.del(TrelloClient.getKeyOAuthSecretKeyForUserToken(userToken));
    }

    public String getOAuthTokenFromUserAuthToken(String userToken) {
        String cached = oAuthCache.getIfPresent(userToken);

        if (cached == null) {
            cached = decrypt(redis.get(getRedisKeyForUserOAuthToken(userToken)));
            if (cached != null) {
                oAuthCache.put(userToken, cached);
            }
        }

        return cached;
    }

    /*
    OAuth tokens are stored in redis encrytped
     */
    private String decrypt(String encryptedToken) {
        if (encryptedToken == null) {
            return null;
        }

        // you'd should use a more 'sophisticated' encryption system
        return new String(Base64.getDecoder().decode(encryptedToken));
    }

    private String encrypt(String oAuthToken) {
        // you'd should use a more 'sophisticated' encryption system
        return new String(Base64.getEncoder().encode(oAuthToken.getBytes()));
    }

    private String getRedisKeyForUserOAuthToken(String userToken) {
        return REDIS_PREFIX_FOR_USER_OAUTH_TOKEN + userToken;
    }
}
