package scott.jeremy;

import org.springframework.http.HttpStatus;

public class HttpStatusException extends RuntimeException {
    private final String message;
    private final HttpStatus status;
    private final Integer errorCode;

    public HttpStatusException(String message, HttpStatus status, Integer errorCode, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.status = status;
        this.errorCode = errorCode;
    }

    public int getStatus() {
        return status.value();
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getHttpMessage() {
        return message;
    }
}
