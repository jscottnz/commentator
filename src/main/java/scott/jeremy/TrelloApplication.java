package scott.jeremy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class TrelloApplication {
	public static void main(String[] args) {
		SpringApplication.run(TrelloApplication.class, args);
	}
}
