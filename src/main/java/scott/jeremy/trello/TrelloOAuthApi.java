package scott.jeremy.trello;

import com.github.scribejava.apis.TrelloApi;
import com.github.scribejava.core.model.OAuth1RequestToken;

public class TrelloOAuthApi extends TrelloApi {

    private final String applicationName;

    public TrelloOAuthApi(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getAuthorizationUrl(OAuth1RequestToken requestToken) {
        return String.format("https://trello.com/1/OAuthAuthorizeToken?name=%s&scope=read,write&oauth_token=%s", applicationName, requestToken.getToken());
    }
}