package scott.jeremy.trello;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import scott.jeremy.HttpStatusException;
import scott.jeremy.Response;
import scott.jeremy.UserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import static scott.jeremy.AuthFilter.AUTH_TOKEN_HEADER;

@CrossOrigin
@RestController
@EnableAutoConfiguration
public class Trello {

    public static final String TRELLO_AUTH_CALLBACK_PATH = "/trello/oauthCallback";

    private UserService userService;
    private TrelloClient trelloClient;
    private final Jedis redis;
    private final String authCompleteCallback;

    public Trello(UserService userService,
                  TrelloClient trelloClient,
                  Jedis redis,
                  String authCompleteCallback) {
        this.userService = userService;
        this.trelloClient = trelloClient;
        this.redis = redis;
        this.authCompleteCallback = authCompleteCallback;
    }

    @RequestMapping("/healthCheck")
    public @ResponseBody Response healthCheck() {
        Map<String, String> map = new HashMap<>();

        try {
            redis.get("healthCheck");
        } catch (Exception e) {
            throw new HttpStatusException("Redis is not connected", HttpStatus.INTERNAL_SERVER_ERROR, null, null);
        }

        map.put("healthCheck", "OK");
        return new Response().data(map);
    }

    @RequestMapping("/authed")
    public @ResponseBody Response authed(@RequestHeader(AUTH_TOKEN_HEADER) String userToken) {
        Map<String, Boolean> map = new HashMap<>();
        map.put("authed", userService.isAuthorized(userToken));
        return new Response().data(map);
    }

    @RequestMapping("/trello/authorizeAndRedirect")
    public void authorizeAndRedirect(HttpServletResponse response,
                                     @RequestParam("ut") String userToken)
            throws InterruptedException, ExecutionException, IOException {
        response.sendRedirect(trelloClient.getAuthorizeUrl(userToken));
    }

    @RequestMapping(TRELLO_AUTH_CALLBACK_PATH)
    public void oAuthCallback(HttpServletResponse response,
                              @RequestParam("ut") String userToken,
                              @RequestParam(value = "oauth_token", defaultValue = "") String oAuthToken,
                              @RequestParam(value = "oauth_verifier", defaultValue = "") String oAuthVerifier)
            throws IOException, ExecutionException, InterruptedException {

        if (oAuthToken.isEmpty()) {
            response.sendRedirect(authCompleteCallback);
            return;
        }

        String accessToken = trelloClient.completeAuthorize(userToken, oAuthToken, oAuthVerifier);
        userService.saveOAuthTokenForUser(userToken, accessToken);
        response.sendRedirect(authCompleteCallback);
    }

    @CrossOrigin
    @RequestMapping("/trello/boards")
    public @ResponseBody Response boards(@RequestHeader(AUTH_TOKEN_HEADER) String userToken) {
        return doRequest(userToken, trelloToken ->  trelloClient.getBoards(trelloToken));
    }

    @RequestMapping("/trello/cards/{boardId}")
    public @ResponseBody Response cards(@RequestHeader(AUTH_TOKEN_HEADER) String userToken,
                                        @PathVariable("boardId") String boardId) {
        return doRequest(userToken, trelloToken ->  trelloClient.getCards(trelloToken, boardId));
    }

    @RequestMapping(value = "/trello/comment/{cardId}", method = RequestMethod.POST)
    public @ResponseBody Response saveComment(@RequestHeader(AUTH_TOKEN_HEADER) String userToken,
                                              @PathVariable("cardId") String cardId,
                                              @RequestBody Map<String, String> json) {
        return doRequest(userToken, trelloToken -> trelloClient.saveComment(trelloToken, cardId, json.get("text")));
    }

    private Response doRequest(String userToken, Function<String, Object> getDataFromTrelloFn) {
        String trelloToken = authenticateTrello(userToken);

        Object data = getDataFromTrelloFn.apply(trelloToken);

        return new Response().data(data);
    }

    private String authenticateTrello(String userToken)  {
        String integrationToken = userService.getOAuthTokenFromUserAuthToken(userToken);

        if (integrationToken == null) {
            throw new HttpStatusException("Please authenticate with Trello", HttpStatus.UNAUTHORIZED, 4001, null);
        }
        return integrationToken;
    }
}
