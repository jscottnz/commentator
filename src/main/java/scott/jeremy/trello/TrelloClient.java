package scott.jeremy.trello;

import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import redis.clients.jedis.Jedis;
import scott.jeremy.HttpStatusException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

public class TrelloClient {
    private static final Logger logger = Logger.getLogger(TrelloClient.class);

    private static final String API_URL = "https://trello.com/1";
    private static final String OAUTH_KEY_PARAM = "key";
    private static final String OAUTH_TOKEN_PARAM = "token";
    private final String key;
    private final Function<String /* userToken */, OAuth10aService> auth10aServiceFactoryFn;
    private final Jedis redis;
    private final Gson gson;

    private static Type trelloItemListType = new TypeToken<List<TrelloItem>>(){}.getType();

    public TrelloClient(String key,
                        Function<String /* userToken */, OAuth10aService> auth10aServiceFactoryFn,
                        Jedis redis,
                        Gson gson) {
        this.key = key;
        this.auth10aServiceFactoryFn = auth10aServiceFactoryFn;
        this.redis = redis;
        this.gson = gson;
    }

    public String getAuthorizeUrl(String userToken) throws InterruptedException, ExecutionException, IOException {
        logger.info("generating authorization url for " + userToken);

        OAuth10aService oAuth10aService = auth10aServiceFactoryFn.apply(userToken);
        OAuth1RequestToken requestToken = getoAuth1RequestToken(oAuth10aService);

        redis.set(getKeyOAuthSecretKeyForUserToken(userToken), requestToken.getTokenSecret());

        return oAuth10aService.getAuthorizationUrl(requestToken);
    }

    public String completeAuthorize(String userToken, String exchangeToken, String oAuthVerifier)
            throws InterruptedException, ExecutionException, IOException {
        logger.info("completing authorization for " + userToken);

        OAuth10aService oAuth10aService = auth10aServiceFactoryFn.apply(userToken);

        String oauthTokenSecret = redis.get(getKeyOAuthSecretKeyForUserToken(userToken));
        if (oauthTokenSecret == null) {
            // No token secret found
            throw new IllegalStateException("An error occured with Trello authentication. Please start again");
        }

        OAuth1RequestToken requestToken = new OAuth1RequestToken(exchangeToken, oauthTokenSecret);
        OAuth1AccessToken accessToken = getoAuth1AccessToken(oAuthVerifier, oAuth10aService, requestToken);

        return accessToken.getToken();
    }

    public List<TrelloItem> getBoards(String trelloToken) {
        String body = callTrello(trelloToken, "/member/me/boards");
        return gson.fromJson(body, trelloItemListType);
    }

    public List<TrelloItem> getCards(String trelloToken, String boardId) {
        String body = callTrello(trelloToken, "/boards/" + boardId + "/cards");
        return gson.fromJson(body, trelloItemListType);
    }

    public String saveComment(String trelloToken, String cardId, String text) {
        if(text == null || text.isEmpty() || text.length() > 16384) {
            throw new HttpStatusException("Comments must be between 1 to 16384 characters", HttpStatus.BAD_REQUEST, 4002, null);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("text", text);
        return postTrello(trelloToken, "/cards/" + cardId + "/actions/comments", data);
    }

    String callTrello(String trelloToken, String path)  {
        logger.info("calling trello " + path);
        try {
            HttpResponse<String> response = getUnirest(API_URL + path)
                    .queryString(OAUTH_KEY_PARAM, key)
                    .queryString(OAUTH_TOKEN_PARAM, trelloToken)
                    .asString();

            if(response.getStatus() != 200) {
                throw new HttpStatusException("Unexpected response from Trello", HttpStatus.valueOf(response.getStatus()), null, null);
            }

            return response.getBody();
        } catch (HttpStatusException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while communicating with Trello", e);
            throw new HttpStatusException("Error while communicating with Trello", HttpStatus.BAD_GATEWAY, 5003, e);
        }
    }

    String postTrello(String trelloToken, String path, Map<String, Object> data)  {
        try {
            logger.info("calling trello " + path);

            data.put(OAUTH_KEY_PARAM, key);
            data.put(OAUTH_TOKEN_PARAM, trelloToken);

            HttpResponse<String> response = postUnirest(API_URL + path)
                    .fields(data)
                    .asString();

            if(response.getStatus() != 200) {
                throw new HttpStatusException("Unexpected response from Trello " + response.getBody(), HttpStatus.valueOf(response.getStatus()), null, null);
            }

            return response.getBody();
        } catch (HttpStatusException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while communicating with Trello", e);
            throw new HttpStatusException("Error while communicating with Trello", HttpStatus.BAD_GATEWAY, 5003, e);
        }
    }

    public static String getKeyOAuthSecretKeyForUserToken(String userToken) {
        return "oauth_secret_" + userToken;
    }

    /*
    For testing
     */
    OAuth1RequestToken getoAuth1RequestToken(OAuth10aService oAuth10aService) throws IOException, InterruptedException, ExecutionException {
        return oAuth10aService.getRequestToken();
    }

    OAuth1AccessToken getoAuth1AccessToken(String oAuthVerifier, OAuth10aService oAuth10aService, OAuth1RequestToken requestToken) throws IOException, InterruptedException, ExecutionException {
        return oAuth10aService.getAccessToken(requestToken, oAuthVerifier);
    }

    GetRequest getUnirest(String path) {
        return Unirest.get(path);
    }

    HttpRequestWithBody postUnirest(String path) {
        return Unirest.post(path);
    }


}
