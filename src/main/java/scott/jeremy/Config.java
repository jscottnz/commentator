package scott.jeremy;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import scott.jeremy.trello.Trello;
import scott.jeremy.trello.TrelloClient;
import scott.jeremy.trello.TrelloOAuthApi;

import java.util.function.Function;

import static scott.jeremy.trello.Trello.TRELLO_AUTH_CALLBACK_PATH;

@Configuration
public class Config {

    private EnvironmentConfig env = new EnvironmentConfig();

    @Bean
    public FilterRegistrationBean exceptionFilter() {
        FilterRegistrationBean exceptionFilter = new FilterRegistrationBean();
        exceptionFilter.setFilter(new ExceptionFilter(gson()));
        exceptionFilter.setOrder(0);

        return exceptionFilter;
    }

    @Bean
    public Trello trello() {
        return new Trello(userService(), trelloClient(), redis(), getEnv().getAuthCompleteCallback());
    }

    @Bean
    public TrelloClient trelloClient() {
        return new TrelloClient(getEnv().getKey(), trelloOAuthServiceFactoryFn(), redis(), gson());
    }

    @Bean
    public UserService userService() {
        Cache<String, String> cache = CacheBuilder
                .newBuilder()
                .maximumSize(2)
                .build();

        return new UserService(cache, redis()) {

            /*
            Simple authorization for demo
             */
            @Override
            public boolean isAuthorized(String token) {
                return !token.startsWith("FAIL");
            }
        };
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public Jedis redis() {
        Jedis redis = new Jedis(getEnv().getRedisHostPort());
        redis.connect();

        if (!redis.isConnected()) {
            throw new RuntimeException("Redis is not available on " + getEnv().getRedisHostPort());
        }

        return redis;
    }

    @Bean
    public Function<String /* userToken */, OAuth10aService> trelloOAuthServiceFactoryFn() {
        return userToken -> new ServiceBuilder()
                .apiKey(getEnv().getKey())
                .apiSecret(getEnv().getSecret())
                .callback(getEnv().getHost() + TRELLO_AUTH_CALLBACK_PATH + "?ut=" + userToken)
                .scope("read,write")
                .build(new TrelloOAuthApi(getEnv().getAppName()));
    }

    public EnvironmentConfig getEnv() {
        return env;
    }
}
