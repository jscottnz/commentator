package scott.jeremy;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;

@WebFilter
public class AuthFilter extends GenericFilterBean {

    public static final String AUTH_TOKEN_HEADER = "X-Auth-Token";

    private HashSet<String> excludeUrls = new HashSet<>();

    private UserService userService;

    public AuthFilter(UserService userService) {
        this.userService = userService;
        excludeUrls.add("/app");
        excludeUrls.add("/healthCheck");
        excludeUrls.add("/trello/oauthCallback");
        excludeUrls.add("/trello/authorizeAndRedirect");
        excludeUrls.add("/authed");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)servletRequest;

        // pass through options requests
        if (httpRequest.getMethod().toLowerCase().equals("options")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        for (String excludeUrl : excludeUrls) {
            if (httpRequest.getRequestURI().startsWith(excludeUrl)) {

                // pass through auth excluded urls
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        }

        String token = httpRequest.getHeader(AUTH_TOKEN_HEADER);
        if (token == null || !userService.isAuthorized(token)) {
            throw new HttpStatusException("Authorization Token expected.", HttpStatus.BAD_REQUEST, 4000, null);
        }

        // auth succeeded, continue
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
