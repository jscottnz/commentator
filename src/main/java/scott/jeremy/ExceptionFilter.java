package scott.jeremy;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExceptionFilter extends GenericFilterBean {

    private static final Logger logger = Logger.getLogger(ExceptionFilter.class);
    private final Gson gson;

    public ExceptionFilter(Gson gson) {
        this.gson = gson;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse)servletResponse;

        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (Exception e) {

            int status;
            String message;
            Integer errorCode = null;
            if (e instanceof HttpStatusException) {
                HttpStatusException e1 = (HttpStatusException)e;
                status = e1.getStatus();
                message = e1.getHttpMessage();
                errorCode = e1.getErrorCode();
            } else if (e.getCause() instanceof HttpStatusException) {
                HttpStatusException e1 = (HttpStatusException)e.getCause();
                status = e1.getStatus();
                message = e1.getHttpMessage();
                errorCode = e1.getErrorCode();
            } else {
                logger.error("Unhandled Exception", e);

                status = 500;
                message = "Sorry. An error occurred";
            }

            Map<String, Object> response = new HashMap<>();
            response.put("status", status);
            response.put("error", message);
            response.put("errorCode", errorCode);

            httpResponse.setStatus(status);
            httpResponse.getWriter().print(gson.toJson(response));
        }
    }
}
