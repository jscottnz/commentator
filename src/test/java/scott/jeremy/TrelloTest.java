package scott.jeremy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import redis.clients.jedis.Jedis;
import scott.jeremy.trello.Trello;
import scott.jeremy.trello.TrelloClient;
import scott.jeremy.trello.TrelloItem;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrelloTest {

    private Trello trello;
    @Mock
    private UserService userService;
    @Mock
    private TrelloClient trelloClient;
    @Mock
    private Jedis redis;
    @Mock
    private HttpServletResponse response;

    @Before
    public void setup() {
        trello = new Trello(userService, trelloClient, redis, "callback");
    }

    @Test
    public void healthCheckOk() {
        Map<String, String> data = (Map<String, String>) trello.healthCheck().getData();
        assertThat(data.get("healthCheck"), is("OK"));
    }

    @Test
    public void healthCheckRedisFailure() {
        when(redis.get(anyString())).thenThrow(new RuntimeException());

        try {
            trello.healthCheck();
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Redis is not connected"));
            assertThat(e.getStatus(), is(500));
        }
    }

    @Test
    public void authed() {
        when(userService.isAuthorized("x")).thenReturn(false);
        when(userService.isAuthorized("y")).thenReturn(true);

        Map<String, Boolean> data = (Map<String, Boolean>) trello.authed("x").getData();
        assertThat(data.get("authed"), is(false));

        data = (Map<String, Boolean>) trello.authed("y").getData();
        assertThat(data.get("authed"), is(true));
    }

    @Test
    public void authorizeAndRedirect() throws InterruptedException, ExecutionException, IOException {
        when(trelloClient.getAuthorizeUrl("x")).thenReturn("url");
        trello.authorizeAndRedirect(response, "x");

        verify(response).sendRedirect("url");
    }

    @Test
    public void oAuthCallback() throws InterruptedException, ExecutionException, IOException {
        when(trelloClient.completeAuthorize("ut", "ot", "ov")).thenReturn("at");

        trello.oAuthCallback(response, "ut", "ot", "ov");

        verify(userService).saveOAuthTokenForUser("ut", "at");
        verify(response).sendRedirect("callback");
    }

    @Test
    public void oAuthCallbackEmpty() throws InterruptedException, ExecutionException, IOException {
        trello.oAuthCallback(response, "ut", "", "");

        verify(response).sendRedirect("callback");
        verify(userService, never()).saveOAuthTokenForUser(any(), any());
    }

    @Test
    public void getBoards() {
        when(userService.getOAuthTokenFromUserAuthToken("ut")).thenReturn("ot");
        List<TrelloItem> data = new ArrayList<TrelloItem>();
        when(trelloClient.getBoards("ot")).thenReturn(data);

        Response boards = trello.boards("ut");
        assertThat(boards.getData(), is(data));
    }

    @Test
    public void getCards() {
        when(userService.getOAuthTokenFromUserAuthToken("ut")).thenReturn("ot");
        List<TrelloItem> data = new ArrayList<TrelloItem>();
        when(trelloClient.getCards("ot", "1234")).thenReturn(data);

        Response cards = trello.cards("ut", "1234");
        assertThat(cards.getData(), is(data));
    }

    @Test
    public void saveComment() {
        when(userService.getOAuthTokenFromUserAuthToken("ut")).thenReturn("ot");
        Map<String, String> m = new HashMap<>();
        m.put("text", "boo");

        trello.saveComment("ut", "1234", m);

        verify(trelloClient).saveComment("ot", "1234", "boo");
    }
}
