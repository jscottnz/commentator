package scott.jeremy.trello;

import com.github.scribejava.core.model.OAuth1RequestToken;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TrelloOAuthApiTest {

    @Test
    public void getAuthorizationUrl() {

        OAuth1RequestToken token = new OAuth1RequestToken("theToken", "");
        String url = new TrelloOAuthApi("hello world").getAuthorizationUrl(token);

        assertThat(url, is("https://trello.com/1/OAuthAuthorizeToken?name=hello world&scope=read,write&oauth_token=theToken"));
    }

}