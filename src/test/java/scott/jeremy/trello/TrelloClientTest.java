package scott.jeremy.trello;

import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import redis.clients.jedis.Jedis;
import scott.jeremy.HttpStatusException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrelloClientTest {

    private static String TOKEN = "oAuthToken";
    private static String AUTH_URL = "authURL";
    private static String TOKEN_SECRET = "secret";
    private static String ACCESS_TOKEN = "accessToken";

    private Gson gson = new Gson();
    private TrelloClient trelloClient;
    @Mock
    private Function<String, OAuth10aService> authFactoryFn;
    @Mock
    private Jedis redis;
    @Mock
    private OAuth10aService oAuthService;
    @Mock
    private OAuth1RequestToken requestToken;
    @Mock
    private OAuth1AccessToken accessToken;

    @Before
    public void setup() throws InterruptedException, ExecutionException, IOException {
        trelloClient = spy(new TrelloClient("appKey", authFactoryFn, redis, gson));

        when(authFactoryFn.apply(TOKEN)).thenReturn(oAuthService);
        doReturn(requestToken).when(trelloClient).getoAuth1RequestToken(oAuthService);
        when(requestToken.getTokenSecret()).thenReturn(TOKEN_SECRET);
        when(oAuthService.getAuthorizationUrl(requestToken)).thenReturn(AUTH_URL);
        when(accessToken.getToken()).thenReturn(ACCESS_TOKEN);
    }

    @Test
    public void shouldParseBoardJson() throws UnirestException {
        String json = "[{\"name\":\"*Tutorial Board (Start Here!)\",\"desc\":\"\",\"descData\":null,\"closed\":false,\"idOrganization\":null,\"pinned\":null,\"invitations\":null,\"memberships\":[{\"idMember\":\"50095233f62adbe04d935195\",\"memberType\":\"normal\",\"_id\":\"58fe07354229ced2ab62417a\",\"deactivatedAtEnterprise\":false,\"deactivated\":false,\"unconfirmed\":false},{\"memberType\":\"admin\",\"idMember\":\"58fe07354229ced2ab624175\",\"_id\":\"58fe07354229ced2ab62417b\",\"deactivatedAtEnterprise\":false,\"deactivated\":false,\"unconfirmed\":false}],\"shortLink\":\"ZmyGI4mY\",\"powerUps\":[],\"dateLastActivity\":\"2017-04-24T14:09:57.640Z\",\"idTags\":[],\"datePluginDisable\":null,\"id\":\"58fe07354229ced2ab624177\",\"invited\":false,\"starred\":false,\"url\":\"https://trello.com/b/ZmyGI4mY/tutorial-board-start-here\",\"prefs\":{\"permissionLevel\":\"private\",\"voting\":\"disabled\",\"comments\":\"members\",\"invitations\":\"members\",\"selfJoin\":true,\"cardCovers\":true,\"calendarFeedEnabled\":false,\"background\":\"sky\",\"backgroundImage\":null,\"backgroundImageScaled\":null,\"backgroundTile\":false,\"backgroundBrightness\":\"dark\",\"backgroundColor\":\"#00AECC\",\"canBePublic\":true,\"canBeOrg\":true,\"canBePrivate\":true,\"canInvite\":true},\"subscribed\":false,\"labelNames\":{\"green\":\"This is a green label\",\"yellow\":\"\",\"orange\":\"\",\"red\":\"\",\"purple\":\"\",\"blue\":\"\",\"sky\":\"\",\"lime\":\"\",\"pink\":\"\",\"black\":\"\"},\"dateLastView\":\"2017-04-24T17:43:00.689Z\",\"shortUrl\":\"https://trello.com/b/ZmyGI4mY\"}]";

        TrelloClient trelloClient = new TrelloClient(null, null, null, gson) {
            @Override
            String callTrello(String trelloToken, String path) {
                return json;
            }
        };

        List<TrelloItem> boards = trelloClient.getBoards(null);

        assertThat(boards, hasSize(1));

        TrelloItem board = boards.get(0);
        assertThat(board.getId(), is("58fe07354229ced2ab624177"));
        assertThat(board.getName(), is("*Tutorial Board (Start Here!)"));
    }

    @Test
    public void shouldParseCardJson() throws UnirestException {
        String json = "[{\"id\":\"58fe07354229ced2ab62417d\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.549Z\",\"desc\":\"\",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":1,\"idAttachmentCover\":\"58fe07354229ced2ab62417e\",\"manualCoverAttachment\":false,\"idLabels\":[],\"name\":\"This is a card. Drag it to the \\\"Tried It\\\" List to show it's done. →\",\"pos\":65535,\"shortLink\":\"Wbtb27CJ\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":0,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":0,\"description\":false,\"due\":null,\"dueComplete\":false},\"dueComplete\":false,\"due\":null,\"idChecklists\":[],\"idMembers\":[],\"labels\":[],\"shortUrl\":\"https://trello.com/c/Wbtb27CJ\",\"subscribed\":false,\"url\":\"https://trello.com/c/Wbtb27CJ/1-this-is-a-card-drag-it-to-the-tried-it-list-to-show-it-s-done\"},{\"id\":\"58fe07354229ced2ab62417f\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.567Z\",\"desc\":\"###Three Trello Basics: \\n\\n**1. Boards**: organize a project or just about anything.\\n**2. Lists**: are super flexible, but often steps like *\\\"To Do, Doing, and Done\\\"*.\\n**3. Cards**: represent tasks or store information. \\n\\n\\nWatch this short video to learn more: \\n\\n[![](https://trello-attachments.s3.amazonaws.com/571936e84bb334420cce0df1/57bdde964803b5a2d3d17f8e/9523b90931678530ec2775ebbdc124d8/this_is_a_trello_board_with_lists_and_cards.jpg)](https://trello.wistia.com/medias/n5tpbc6uo1)\\n----\\n\\n--- \\n\\nYou're inside the *Tutorial (Start Here!)* **board** right now. \\n\\nYou're reading a **card** called *The Trello Basics*.\\n\\nEverything in this *Stuff to Try* **list** is a **card**. \\n\\nYou can add as many **lists** and **cards** to a **board** as you want.\\n\\n----- \\n\\n\\n**Click** the \\\"X\\\" in the **top right corner** of this **card** to close the card and **drag it** over to the *\\\"Tried It\\\"* **list** because you're all done! \uD83C\uDF89\",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":2,\"idAttachmentCover\":\"58fe07354229ced2ab62417e\",\"manualCoverAttachment\":true,\"idLabels\":[],\"name\":\"Trello Basics [CLICK TO OPEN]\",\"pos\":131071,\"shortLink\":\"24Ddpfre\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":0,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":1,\"description\":true,\"due\":null,\"dueComplete\":false},\"dueComplete\":false,\"due\":null,\"idChecklists\":[],\"idMembers\":[],\"labels\":[],\"shortUrl\":\"https://trello.com/c/24Ddpfre\",\"subscribed\":false,\"url\":\"https://trello.com/c/24Ddpfre/2-trello-basics-click-to-open\"},{\"id\":\"58fe07354229ced2ab624187\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.589Z\",\"desc\":\"- Click *Edit* ↑ to edit a card's description. To change a card's title just click it!\\n\\n- Use the buttons on the right to add pictures, labels, checklists and more. →\\n\\n- The attached picture is Taco, the most famous Siberian Husky in New York City, and official spokes-mascot for Trello. You'll see him around Trello introducing new features and whatnot. \",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":3,\"idAttachmentCover\":\"58fe07354229ced2ab624188\",\"manualCoverAttachment\":false,\"idLabels\":[\"58fe07354229ced2ab624189\"],\"name\":\"Mastering Trello Cards\",\"pos\":196607,\"shortLink\":\"VoRxLH8T\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":6,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":1,\"description\":true,\"due\":\"2020-06-24T16:00:00.000Z\",\"dueComplete\":false},\"dueComplete\":false,\"due\":\"2020-06-24T16:00:00.000Z\",\"idChecklists\":[\"58fe07354229ced2ab6241ab\"],\"idMembers\":[],\"labels\":[{\"id\":\"58fe07354229ced2ab624189\",\"idBoard\":\"58fe07354229ced2ab624177\",\"name\":\"This is a green label\",\"color\":\"green\",\"uses\":1}],\"shortUrl\":\"https://trello.com/c/VoRxLH8T\",\"subscribed\":false,\"url\":\"https://trello.com/c/VoRxLH8T/3-mastering-trello-cards\"},{\"id\":\"58fe07354229ced2ab624192\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.606Z\",\"desc\":\"###Trello creates a shared perspective.\\n\\nTrello boards get everyone on the same page on Design Projects, Editorial Calendars, Event Planning, Sales Pipelines, Recruiting, and more!\\n\\nInvite colleagues, family, and friends to boards to collaborate.\\n\\nTo see all the ways Trello can help: trello.com/inspiration \\n\\n---\\n\\n![](https://trello-attachments.s3.amazonaws.com/571936e84bb334420cce0df1/57bdde964803b5a2d3d17f8e/b03c8770438c3a03f43b530bc088865f/Create_New_Board.gif)\\n\\n\",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":4,\"idAttachmentCover\":\"58fe07354229ced2ab62417e\",\"manualCoverAttachment\":false,\"idLabels\":[],\"name\":\"Create Your First Board\",\"pos\":262143,\"shortLink\":\"7KE0ZOO5\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":5,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":0,\"description\":true,\"due\":null,\"dueComplete\":false},\"dueComplete\":false,\"due\":null,\"idChecklists\":[\"58fe07354229ced2ab6241bb\"],\"idMembers\":[],\"labels\":[],\"shortUrl\":\"https://trello.com/c/7KE0ZOO5\",\"subscribed\":false,\"url\":\"https://trello.com/c/7KE0ZOO5/4-create-your-first-board\"},{\"id\":\"58fe07354229ced2ab624194\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.624Z\",\"desc\":\"###Trello is a flexible platform for your whole [team](http://help.trello.com/article/927-what-are-teams) to collaborate and get stuff done. \\n\\n\\n- **Power-Ups** turn Trello boards into living applications, with additional features & integrations from the apps you love! trello.com/power-ups :rocket:\\n\\n![](https://trello-attachments.s3.amazonaws.com/57c71ac1247e638ba40f3979/600x300/0c916c343233dc8456fbe31aedbfef1d/Power-Up_Scroll_50.gif)\\n\\n- Bit of a tech wiz? Use our open API to build your own integrations: developers.trello.com\\n\\n- Also, check out these browser extensions and add-ons: trello.com/integrations\",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":5,\"idAttachmentCover\":\"58fe07354229ced2ab62417e\",\"manualCoverAttachment\":true,\"idLabels\":[],\"name\":\"Power-Ups, Integrations, APIs, oh my!\",\"pos\":327679,\"shortLink\":\"5hKt7AGP\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":0,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":1,\"description\":true,\"due\":null,\"dueComplete\":false},\"dueComplete\":false,\"due\":null,\"idChecklists\":[],\"idMembers\":[],\"labels\":[],\"shortUrl\":\"https://trello.com/c/5hKt7AGP\",\"subscribed\":false,\"url\":\"https://trello.com/c/5hKt7AGP/5-power-ups-integrations-apis-oh-my\"},{\"id\":\"58fe07354229ced2ab62419b\",\"checkItemStates\":null,\"closed\":false,\"dateLastActivity\":\"2017-04-24T14:09:57.640Z\",\"desc\":\"- Visit the **[Trello Guide](https://trello.com/guide)** to get started, get inspired, and learn more.\\n\\n- The **[Trello Blog](https://blog.trello.com)** has regular updates, productivity tips, and workflows. \\n\\n- Download Trello for **[iOS](https://itunes.apple.com/app/trello-organize-anything/id461504587)** and **[Android](https://play.google.com/store/apps/details?id=com.trello)** to stay in sync when you're on the go. \\n\\n- Become a Trello productivity pro! **[Join a free webinar.](https://trello.com/webinars?utm_source=trello&utm_medium=welcome_board&utm_campaign=webinars\\n)** \\n\\n- Questions? Visit **help.trello.com**. Or **[contact us](https://trello.com/contact)**. We're happy to help! \uD83D\uDE0E\\n\\n\",\"descData\":null,\"idBoard\":\"58fe07354229ced2ab624177\",\"idList\":\"58fe07354229ced2ab624178\",\"idMembersVoted\":[],\"idShort\":6,\"idAttachmentCover\":\"58fe07354229ced2ab62417e\",\"manualCoverAttachment\":false,\"idLabels\":[],\"name\":\"Help, Pro Tips, Webinars & More\",\"pos\":393215,\"shortLink\":\"dsp4hWfP\",\"badges\":{\"votes\":0,\"viewingMemberVoted\":false,\"subscribed\":false,\"fogbugz\":\"\",\"checkItems\":0,\"checkItemsChecked\":0,\"comments\":0,\"attachments\":0,\"description\":true,\"due\":null,\"dueComplete\":false},\"dueComplete\":false,\"due\":null,\"idChecklists\":[],\"idMembers\":[],\"labels\":[],\"shortUrl\":\"https://trello.com/c/dsp4hWfP\",\"subscribed\":false,\"url\":\"https://trello.com/c/dsp4hWfP/6-help-pro-tips-webinars-more\"}]";

        TrelloClient trelloClient = new TrelloClient(null, null, null, gson) {
            @Override
            String callTrello(String trelloToken, String path) {
                return json;
            }
        };

        List<TrelloItem> cards = trelloClient.getCards(null, null);

        assertThat(cards, hasSize(6));

        TrelloItem card = cards.get(0);
        assertThat(card.getId(), is("58fe07354229ced2ab62417d"));
        assertThat(card.getName(), is("This is a card. Drag it to the \"Tried It\" List to show it's done. →"));
    }

    @Test
    public void getAuthorizeUrlShouldSaveToRedis() throws InterruptedException, ExecutionException, IOException {
        String url = trelloClient.getAuthorizeUrl(TOKEN);

        assertThat(url, is(AUTH_URL));

        verify(redis).set("oauth_secret_oAuthToken", TOKEN_SECRET);
    }

    @Test
    public void completeAuthorizationShouldGetAccessToken() throws InterruptedException, ExecutionException, IOException {
        when(redis.get("oauth_secret_oAuthToken")).thenReturn(TOKEN_SECRET);
        doReturn(accessToken).when(trelloClient).getoAuth1AccessToken(eq("av"), eq(oAuthService), any());

        String token = trelloClient.completeAuthorize(TOKEN, "et", "av");

        assertThat(token, is(ACCESS_TOKEN));
    }

    @Test(expected = IllegalStateException.class)
    public void completeAuthorizationShouldThrowIfNotTokenInReids() throws InterruptedException, ExecutionException, IOException {
        when(redis.get("oauth_secret_oAuthToken")).thenReturn(null);

        trelloClient.completeAuthorize(TOKEN, "et", "av");
    }

    @Test
    public void saveComment() {
        doReturn("ok").when(trelloClient).postTrello(eq(TOKEN), eq("/cards/1234/actions/comments"), any());

        String response = trelloClient.saveComment(TOKEN, "1234", "hello");

        assertThat(response, is("ok"));
    }

    @Test
    public void saveCommentShouldThrowWithInvalidText() {
        try {
            trelloClient.saveComment(TOKEN, "1234", "");
            fail("exception expected");
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Comments must be between 1 to 16384 characters"));
            assertThat(e.getStatus(), is(400));
            assertThat(e.getErrorCode(), is(4002));
        }
    }

    @Test
    public void testCallTrello() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        GetRequest getRequest = mock(GetRequest.class);
        when(trelloClient.getUnirest(any())).thenReturn(getRequest);
        when(getRequest.queryString(anyString(), anyString())).thenReturn(getRequest);

        HttpResponse<String> response = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(response);
        when(response.getStatus()).thenReturn(200);
        when(response.getBody()).thenReturn("body");

        String body = trelloClient.callTrello(TOKEN, "/path");
        assertThat(body, is("body"));

        verify(getRequest).queryString("key", "appKey");
        verify(getRequest).queryString("token", TOKEN);
    }

    @Test
    public void testCallTrelloThrowOnBadStatus() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        GetRequest getRequest = mock(GetRequest.class);
        when(trelloClient.getUnirest(any())).thenReturn(getRequest);
        when(getRequest.queryString(anyString(), anyString())).thenReturn(getRequest);

        HttpResponse<String> response = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(response);
        when(response.getStatus()).thenReturn(400);

        try {
            trelloClient.callTrello(TOKEN, "/path");
            fail("exception expected");
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Unexpected response from Trello"));
            assertThat(e.getStatus(), is(400));
        }

        verify(getRequest).queryString("key", "appKey");
        verify(getRequest).queryString("token", TOKEN);
    }

    @Test
    public void testCallTrelloThrowOnException() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        GetRequest getRequest = mock(GetRequest.class);
        when(trelloClient.getUnirest(any())).thenReturn(getRequest);
        when(getRequest.queryString(anyString(), anyString())).thenReturn(getRequest);

        when(getRequest.asString()).thenThrow(new RuntimeException());

        try {
            trelloClient.callTrello(TOKEN, "/path");
            fail("exception expected");
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Error while communicating with Trello"));
            assertThat(e.getStatus(), is(502));
        }

        verify(getRequest).queryString("key", "appKey");
        verify(getRequest).queryString("token", TOKEN);
    }

    @Test
    public void testPostTrello() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        HttpRequestWithBody postRequest = mock(HttpRequestWithBody.class);
        MultipartBody postRequest1 = mock(MultipartBody.class);
        when(trelloClient.postUnirest(any())).thenReturn(postRequest);
        when(postRequest.fields(any())).thenReturn(postRequest1);
        HttpResponse<String> response = mock(HttpResponse.class);
        when(postRequest1.asString()).thenReturn(response);
        when(response.getStatus()).thenReturn(200);
        when(response.getBody()).thenReturn("body");

        Map m = mock(Map.class);

        String body = trelloClient.postTrello(TOKEN, "/path", m);
        assertThat(body, is("body"));

        verify(postRequest).fields(m);
        verify(m).put("token", TOKEN);
        verify(m).put("key", "appKey");
    }

    @Test
    public void testPostTrelloThrowsOnBadStatus() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        HttpRequestWithBody postRequest = mock(HttpRequestWithBody.class);
        MultipartBody postRequest1 = mock(MultipartBody.class);
        when(trelloClient.postUnirest(any())).thenReturn(postRequest);
        when(postRequest.fields(any())).thenReturn(postRequest1);
        HttpResponse<String> response = mock(HttpResponse.class);
        when(postRequest1.asString()).thenReturn(response);
        when(response.getStatus()).thenReturn(500);
        when(response.getBody()).thenReturn("body");


        Map m = mock(Map.class);

        try {
            trelloClient.postTrello(TOKEN, "/path", m);
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Unexpected response from Trello body"));
            assertThat(e.getStatus(), is(500));
        }

        verify(postRequest).fields(m);
        verify(m).put("token", TOKEN);
        verify(m).put("key", "appKey");
    }

    @Test
    public void testPostTrelloThrowsException() throws UnirestException {
        trelloClient = spy(new TrelloClient("appKey", null, null, null));
        HttpRequestWithBody postRequest = mock(HttpRequestWithBody.class);
        MultipartBody postRequest1 = mock(MultipartBody.class);
        when(trelloClient.postUnirest(any())).thenReturn(postRequest);
        when(postRequest.fields(any())).thenReturn(postRequest1);
        HttpResponse<String> response = mock(HttpResponse.class);
        when(postRequest1.asString()).thenThrow(new RuntimeException());

        Map m = mock(Map.class);

        try {
            trelloClient.postTrello(TOKEN, "/path", m);
        } catch (HttpStatusException e) {
            assertThat(e.getMessage(), is("Error while communicating with Trello"));
            assertThat(e.getStatus(), is(502));
        }

        verify(postRequest).fields(m);
        verify(m).put("token", TOKEN);
        verify(m).put("key", "appKey");
    }
}
