package scott.jeremy;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.junit.Test;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Test
    public void getAndSaveTokens() {
        Cache<String, String> cache = spy(CacheBuilder.newBuilder().build());
        Jedis redis = mock(Jedis.class);

        UserService service = new UserService(cache, redis);


        // empty case
        String token = service.getOAuthTokenFromUserAuthToken("ut");

        verify(cache).getIfPresent("ut");

        assertThat(token, is(nullValue()));
        Mockito.reset(cache, redis);

        // save
        service.saveOAuthTokenForUser("ut", "ot");

        verify(redis).set("oAuthToken_ut", "b3Q=");
        verify(cache).put("ut", "ot");
        verify(redis).del("oauth_secret_ut");
        Mockito.reset(cache, redis);

        //get
        token = service.getOAuthTokenFromUserAuthToken("ut");
        assertThat(token, is("ot"));
        verify(redis, never()).get(anyString());

        //get not cached
        doReturn(null).when(cache).getIfPresent("ut");
        when(redis.get("oAuthToken_ut")).thenReturn("b3Q=");
        token = service.getOAuthTokenFromUserAuthToken("ut");
        assertThat(token, is("ot"));
        verify(redis).get("oAuthToken_ut");
    }

}